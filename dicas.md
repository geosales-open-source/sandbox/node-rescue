
## Primeiros passos
___
### Hello World
Para verificarmos se está tudo bem com nossa instalação do `node` podemos fazer um pequeno teste com o `Hello World` presente em seu site.

1. Criamos um arquivo `.js`
2. Vamos colocar o seguinte código para teste:
    ```js
    const http = require('http');
    
    const hostname = '127.0.0.1';
    const port = 3000;
    
    const server = http.createServer((req, res) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Hello World');
    });
    
    server.listen(port, hostname, () => {
      console.log(`Server running at http://${hostname}:${port}/`);
    });
    ```
3. Vamos abrir um terminal na pasta onde está nosso arquivo `.js` e executar o comando `node nome_do_arquivo.js`.
4. Após executar o comando, vamos abrir o navegador no endereço http://localhost:3000 onde deverá ser exibida a mensagem `"Hello World"`!
