
# NODE-RESCUE
___
Este projeto tem como intuito facilitar a introdução do uso de `node.js` assim como trazer exemplos de sua utilização.


# Instalação
___
Como primeiro passo devemos instalar o `node.js` em nosso ambiente, existem diversos modos para tal, iremos seguir pelo mais básico:
- Iremos baixar a versão mais atual disponível no site do [node.js](https://nodejs.org/en/download/current/), no momento da escrita desta _doc_ a versão se encontra na `18.1.0`.

_OBS:_ a instalação segue bem o padrão Windows, _next_ -> _next_ -> _next_ -> _install_

![img.png](documents/images/node-install.png)

Após finalizar a instalação já podemos partir para nosso primeiro exemplo prático com `node.js`.

[Aqui](dicas.md) podemos encontrar alguns tutoriais e testes que realizamos para ganhar conhecimento sobre a linguagem.