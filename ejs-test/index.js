import express from 'express'

let app = express()

app.set('view engine', 'ejs')

app.get('/',(req, res) => {
    res.render("../templates/page");
})

app.listen(8080, () => {console.log("Servidor iniciou na porta 8080")})